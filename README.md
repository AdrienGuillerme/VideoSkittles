# Android project : video library
February - March 2017
## About 
This video library application was done as a class project for our Android development initiation course.  
This app remains unfinished, as the group separated, but the main features are functionnal. 
You can browse for movies or series, view infos and add them to your watchlist.  

## Installation
The uploaded project contains the sources to build the application. (We used Android studio, but you can build it directly with gradle)  
To use this app, you will need a [TMDB api key](https://www.themoviedb.org/documentation/api) and therefore a TMDB account. Simply put your api key in the `Constants.java` file located at `.\Skittle\app\src\main\java\isen\skittle\utils\`.  
