package isen.skittle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import isen.skittle.database.SkittleDatabaseManager;
import isen.skittle.utils.Constants;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText searchText;
    public ImageView posterView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        searchText = (EditText) findViewById(R.id.search_bar);
        findViewById(R.id.search_button).setOnClickListener(this);
    }

    public void onFilm_clicked(View view) {
        startActivity(new Intent(getApplicationContext(), FilmListActivity.class));

    }

    public void onSeries_clicked(View view) {
        startActivity(new Intent(getApplicationContext(), SeriesListActivity.class));
    }

    @Override
    public void onClick(View view) {
        if (TextUtils.isEmpty(searchText.getText())) {
            Toast.makeText(this, R.string.error_no_search, Toast.LENGTH_LONG).show();
            return;
        }
        final Intent homeIntent = getSearchActivityIntent(searchText.getText().toString());
        startActivity(homeIntent);
    }

    private Intent getSearchActivityIntent(String query) {
        final Intent intent = new Intent(this, SearchActivity.class);
        final Bundle extras = new Bundle();
        extras.putString(Constants.General.SEARCH, query);
        intent.putExtras(extras);
        return intent;
    }
}
