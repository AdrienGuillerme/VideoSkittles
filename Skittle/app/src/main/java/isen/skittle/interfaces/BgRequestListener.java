package isen.skittle.interfaces;

import android.graphics.Bitmap;

/**
 * Created by docte on 04/03/2017.
 */

public interface BgRequestListener {

    void onBgRequestAnswer(Bitmap bitmap);

}
