package isen.skittle.interfaces;

import isen.skittle.api.Movie;

/**
 * Created by docte on 04/03/2017.
 */

public interface MovieRequestListener {

    void onMovieRequestAnswer(Movie movie);

}
