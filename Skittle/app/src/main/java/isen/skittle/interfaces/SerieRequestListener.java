package isen.skittle.interfaces;

import isen.skittle.api.Serie;

/**
 * Created by docte on 04/03/2017.
 */

public interface SerieRequestListener {

    void onSerieRequestAnswer(Serie serie);

}
