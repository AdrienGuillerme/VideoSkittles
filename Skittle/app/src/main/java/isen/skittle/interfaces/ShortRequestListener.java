package isen.skittle.interfaces;

import isen.skittle.api.Results;

/**
 * Created by docte on 04/03/2017.
 */

public interface ShortRequestListener {

    void onShortRequestAnswer(Results results);

}
