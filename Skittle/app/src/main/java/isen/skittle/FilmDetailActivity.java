package isen.skittle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import isen.skittle.api.Movie;
import isen.skittle.async.GetBackground;
import isen.skittle.async.GetMovie;
import isen.skittle.async.GetPoster;
import isen.skittle.database.SkittleDatabaseContract;
import isen.skittle.database.SkittleDatabaseManager;
import isen.skittle.interfaces.MovieRequestListener;
import isen.skittle.utils.Constants;

/**
 * Created by adrie on 03/03/2017.
 */

public class FilmDetailActivity extends AppCompatActivity implements MovieRequestListener, View.OnClickListener{

    private Movie dbmovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.film_details_layout);
        findViewById(R.id.movie_add_button).setOnClickListener(this);
        findViewById(R.id.movie_add_section).setVisibility(View.VISIBLE);
        findViewById(R.id.movie_del_section).setVisibility(View.INVISIBLE);

        int id = 0;
        // Retrieve the id passed as parameter
        final Intent intent = getIntent();
        if (null != intent) {
            final Bundle extras = intent.getExtras();
            if ((null != extras) && (extras.containsKey(Constants.General.ID))) {
                // Retrieve the id
                id = extras.getInt(Constants.General.ID);
                // Set as ActionBar subtitle
                getSupportActionBar().setSubtitle("Movie ID : " + id);
                GetMovie getMovie = new GetMovie(this);
                getMovie.execute(id);
            }
        }
    }

    @Override
    public void onMovieRequestAnswer(Movie movie) {
        this.dbmovie = movie;
        if(SkittleDatabaseManager.doesContainMovie(movie)){
            findViewById(R.id.movie_add_section).setVisibility(View.INVISIBLE);
            findViewById(R.id.movie_del_section).setVisibility(View.VISIBLE);
        }
        ((TextView) findViewById(R.id.movie_title)).setText(movie.title);
        ((TextView) findViewById(R.id.movie_actors)).setText(movie.actors);
        ((TextView) findViewById(R.id.movie_date)).setText(movie.airDate);
        ((TextView) findViewById(R.id.movie_overview)).setText(movie.overview);
        ((TextView) findViewById(R.id.movie_country)).setText(movie.countriesIso);
        ((TextView) findViewById(R.id.movie_runtime)).setText(String.valueOf(movie.runtime) + "min");
        ((TextView) findViewById(R.id.movie_genre)).setText(movie.genresString);
        ((TextView) findViewById(R.id.movie_director)).setText(movie.directors);
        ((TextView) findViewById(R.id.movie_rating)).setText("Rating : "+String.valueOf(movie.rating) + "/10");

        GetPoster getPoster = new GetPoster((ImageView) findViewById(R.id.movie_poster));
        getPoster.execute(movie.posterPath);
        GetBackground getBackground = new GetBackground((ImageView) findViewById(R.id.movie_backdrop));
        getBackground.execute(movie.bgPath);
    }


    @Override
    public void onClick(View v) {
        SkittleDatabaseManager dbManager = new SkittleDatabaseManager();
        SkittleApplication.getContext().getContentResolver().insert(SkittleDatabaseContract.MOVIES_URI, dbManager.movieToContentValues(dbmovie));
        Toast.makeText(this, R.string.add_movie_to_list, Toast.LENGTH_LONG).show();
        findViewById(R.id.movie_add_section).setVisibility(View.INVISIBLE);
        findViewById(R.id.movie_del_section).setVisibility(View.VISIBLE);
    }
}
