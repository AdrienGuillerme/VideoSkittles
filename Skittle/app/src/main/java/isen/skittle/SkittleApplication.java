package isen.skittle;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import isen.skittle.holder.ShortViewHolder;
import isen.skittle.utils.Constants;

/**
 * Created by adrie on 04/03/2017.
 */

public class SkittleApplication extends Application {

    private static Context sContext;

    public void onCreate() {
        super.onCreate();

        // Keep a reference to the application context
        sContext = getApplicationContext();
    }

    // Used to access Context anywhere within the app
    public static Context getContext() {
        return sContext;
    }

}
