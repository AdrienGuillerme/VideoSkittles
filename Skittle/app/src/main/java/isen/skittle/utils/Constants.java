package isen.skittle.utils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by docte on 04/03/2017.
 */

public class Constants {

    public class General {
        public static final String SEARCH = "search";
        public static final String LOG_TAG = "Skittle";
        public static final String FILMDB = "filmQuery";
        public static final String ID = "0";
    }


    public class Url {
		public static final String tmdbAPIkey = "1234567890abcdef1234567890abcdef" //put your TMDB API key here
        public static final String baseMultiURL = "https://api.themoviedb.org/3/search/multi?api_key="+ tmdbAPIkey + "&query=";
        public static final String baseMovieURL = "https://api.themoviedb.org/3/movie/%1$d?api_key=" + tmdbAPIkey;
        public static final String baseSerieURL = "https://api.themoviedb.org/3/tv/%1$d?api_key=" + tmdbAPIkey;
        public static final String basePosterURL = "https://image.tmdb.org/t/p/w342/";
        public static final String baseBgURL = "https://image.tmdb.org/t/p/w500/";
        public static final String actorsURL = "http://www.omdbapi.com/?r=json&t=";
    }

    public class Short {
        public static final String TITLE = "title";
        public static final String POSTER = "title";
        public static final String DATE = "date";
        public static final String GENRE = "genre";
        public static final String OVERVIEW = "overview";
        public static final String DIRECTOR = "director";
        public static final String MEDIATYPE = "media type";
        public static final String SCORE = "score";
    }

    public static class Genres {
        public static final List<String> genreList = Arrays.asList("28", "Action",
                "10759", "Action & Adventure",
                "12", "Adventure",
                "16", "Animation",
                "35", "Comedy",
                "80", "Crime",
                "99", "Documentary",
                "18", "Drama",
                "10751", "Family",
                "14", "Fantasy",
                "36", "History",
                "27", "Horror",
                "10762", "Kids",
                "10402", "Music",
                "9648", "Mystery",
                "10763", "News",
                "10764", "Reality",
                "10749", "Romance",
                "10765", "Sci-Fi & Fantasy",
                "878", "Science Fiction",
                "10766", "Soap",
                "10767", "Talk",
                "10770", "TV Movie",
                "53", "Thriller",
                "10752", "War",
                "10768", "War & Politics",
                "37", "Western"
        );
    }
}