package isen.skittle;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import isen.skittle.api.Serie;
import isen.skittle.async.GetBackground;
import isen.skittle.async.GetPoster;
import isen.skittle.async.GetSerie;
import isen.skittle.database.SkittleDatabaseContract;
import isen.skittle.database.SkittleDatabaseManager;
import isen.skittle.interfaces.SerieRequestListener;
import isen.skittle.utils.Constants;

/**
 * Created by adrie on 03/03/2017.
 */

public class SeriesDetailActivity extends AppCompatActivity implements SerieRequestListener, View.OnClickListener{

    private Serie dbserie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.series_details_layout);
        findViewById(R.id.series_add_button).setOnClickListener(this);
        findViewById(R.id.series_add_section).setVisibility(View.VISIBLE);
        findViewById(R.id.series_del_section).setVisibility(View.INVISIBLE);

        int id = 0;
        // Retrieve the id passed as parameter
        final Intent intent = getIntent();
        if (null != intent) {
            final Bundle extras = intent.getExtras();
            if ((null != extras) && (extras.containsKey(Constants.General.ID))) {
                // Retrieve the id
                id = extras.getInt(Constants.General.ID);
                // Set as ActionBar subtitle
                getSupportActionBar().setSubtitle("Series ID : " + id);
                GetSerie getSerie = new GetSerie(this);
                getSerie.execute(id);
            }
        }
    }

    @Override
    public void onSerieRequestAnswer(Serie serie) {
        this.dbserie = serie;
        if(SkittleDatabaseManager.doesContainSerie(serie)){
            findViewById(R.id.series_progress).setVisibility(View.VISIBLE);
            findViewById(R.id.series_add_section).setVisibility(View.INVISIBLE);
            findViewById(R.id.series_del_section).setVisibility(View.VISIBLE);
        }

        ((TextView) findViewById(R.id.series_name)).setText(serie.name);
        ((TextView) findViewById(R.id.series_actors)).setText(serie.actors);
        ((TextView) findViewById(R.id.series_date)).setText(serie.airDate);
        ((TextView) findViewById(R.id.series_overview)).setText(serie.overview);
        ((TextView) findViewById(R.id.series_rating)).setText("Rating : "+String.valueOf(serie.rating) + "/10");
        ((TextView) findViewById(R.id.series_country)).setText(String.valueOf(serie.countriesIso));
        ((TextView) findViewById(R.id.series_writer)).setText(String.valueOf(serie.writers));
        ((TextView) findViewById(R.id.series_genre)).setText(String.valueOf(serie.genresString));
        GetPoster getPoster = new GetPoster((ImageView) findViewById(R.id.series_poster));
        getPoster.execute(serie.posterPath);
        GetBackground getBackground = new GetBackground((ImageView) findViewById(R.id.series_backdrop));
        getBackground.execute(serie.bgPath);
    }

    @Override
    public void onClick(View view) {
        SkittleDatabaseManager dbManager = new SkittleDatabaseManager();
        SkittleApplication.getContext().getContentResolver().insert(SkittleDatabaseContract.SERIES_URI, dbManager.serieToContentValues(dbserie));
        Toast.makeText(this, R.string.add_series_to_list, Toast.LENGTH_LONG).show();
        findViewById(R.id.series_progress).setVisibility(View.VISIBLE);
        findViewById(R.id.series_add_section).setVisibility(View.INVISIBLE);
        findViewById(R.id.series_del_section).setVisibility(View.VISIBLE);
    }
}
