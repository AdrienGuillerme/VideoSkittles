package isen.skittle.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;

import isen.skittle.api.Actor;
import isen.skittle.api.Movie;
import isen.skittle.api.Results;
import isen.skittle.api.Serie;
import isen.skittle.utils.Constants;

/**
 * Created by docte on 04/03/2017.
 */

public class TMDBHelper {

    public static Bitmap getBackground(String imageUrl) throws Exception {
        final HttpURLConnection connection = getHTTPUrlConnection(Constants.Url.baseBgURL + imageUrl);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        final int responseCode = connection.getResponseCode();
        Log.i("TMDB", "getBackground: request sent ! with response code : " + responseCode);
        // If success
        if (responseCode == 200) {
            final Bitmap bitmap = BitmapFactory.decodeStream(connection.getInputStream());
            return bitmap;
        }
        return null;
    }

    public static Bitmap getPoster(String imageUrl) throws Exception {
        final HttpURLConnection connection = getHTTPUrlConnection(Constants.Url.basePosterURL + imageUrl);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        final int responseCode = connection.getResponseCode();
        Log.i("TMDB", "getPoster: request sent ! with response code : " + responseCode);
        // If success
        if (responseCode == 200) {
            final Bitmap bitmap = BitmapFactory.decodeStream(connection.getInputStream());
            return bitmap;
        }
        return null;
    }

    public static Results getShortType(String query) throws Exception {
        final HttpURLConnection connection = getHTTPUrlConnection(query);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("Content-Type", "application/json");
        final int responseCode = connection.getResponseCode();
        Log.i("TMDB", "getShortType: request sent ! with response code : " + responseCode);
        // If success
        if (responseCode == 200) {
            // Build our Tweet list
            final Type type = new TypeToken<Results>() {
            }.getType();
            return new Gson().fromJson(new JsonReader(new InputStreamReader(connection.getInputStream(), "UTF-8")), type);
        }
        return null;
    }

    public static Movie getMovieDetails(int id) throws Exception {
        final HttpURLConnection connection = getHTTPUrlConnection(String.format(Constants.Url.baseMovieURL, id));
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("Content-Type", "application/json");
        final int responseCode = connection.getResponseCode();
        Log.i("TMDB", "getMovieDetails: request sent ! with response code : " + responseCode);
        // If success
        if (responseCode == 200) {
            // Build our Tweet list
            final Type type = new TypeToken<Movie>() {
            }.getType();
            Movie movie = new Gson().fromJson(new JsonReader(new InputStreamReader(connection.getInputStream(), "UTF-8")), type);

            final HttpURLConnection reConnection = getHTTPUrlConnection(Constants.Url.actorsURL + movie.title.replace(' ', '+')  + "&y=" + movie.airDate.substring(0, 4));
            reConnection.setRequestMethod("GET");
            reConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
            reConnection.setRequestProperty("Content-Type", "application/json");
            final int responseCode2 = reConnection.getResponseCode();
            Log.i("TMDB", "getActorDetails: actor request sent ! with response code : " + responseCode);

            if (responseCode2 == 200) {
                final Type type2 = new TypeToken<Actor>() {
                }.getType();
                Actor actor = new Gson().fromJson(new JsonReader(new InputStreamReader(reConnection.getInputStream(), "UTF-8")), type2);
                movie.setActor(actor.actorList);
                movie.setDirectors(actor.directorList);
                movie.setCountriesToString();
                movie.setGenresString();
            }
            return movie;
        }
        return null;
    }

    public static Serie getSerieDetails(int id) throws Exception {
        final HttpURLConnection connection = getHTTPUrlConnection(String.format(Constants.Url.baseSerieURL, id));
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("Content-Type", "application/json");
        final int responseCode = connection.getResponseCode();
        Log.i("TMDB", "getSerieDetails: request sent ! with response code : " + responseCode);
        // If success
        if (responseCode == 200) {
            // Build our Tweet list
            final Type type = new TypeToken<Serie>() {
            }.getType();
            Serie serie = new Gson().fromJson(new JsonReader(new InputStreamReader(connection.getInputStream(), "UTF-8")), type);

            final HttpURLConnection reConnection = getHTTPUrlConnection(Constants.Url.actorsURL + serie.name.replace(' ', '+') + "&y=" + serie.airDate.substring(0, 4));
            reConnection.setRequestMethod("GET");
            reConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
            reConnection.setRequestProperty("Content-Type", "application/json");
            final int responseCode2 = reConnection.getResponseCode();
            Log.i("TMDB", "getActorDetails: actor request sent ! with response code : " + responseCode);

            if (responseCode2 == 200) {
                final Type type2 = new TypeToken<Actor>() {
                }.getType();
                Actor actor = new Gson().fromJson(new JsonReader(new InputStreamReader(reConnection.getInputStream(), "UTF-8")), type2);
                serie.setActor(actor.actorList);
                serie.setWriter(actor.writerList);
                serie.setGenresString();
                serie.setCountriesToString();
            }
            return serie;
        }
        return null;
    }


    private static HttpURLConnection getHTTPUrlConnection(String url) throws Exception {
        return (HttpURLConnection) new URL(url).openConnection();
    }
}
