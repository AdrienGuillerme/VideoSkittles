package isen.skittle.holder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import isen.skittle.R;

/**
 * Created by adrie on 08/03/2017.
 */

public class MovieViewHolder {
    public ImageView coverImage;
    public TextView type;
    public TextView name;
    public TextView genre;
    public TextView date;
    public TextView score;
    public int id;

    public MovieViewHolder(View view){
        coverImage = (ImageView) view.findViewById(R.id.short_poster);
        type = (TextView) view.findViewById(R.id.short_media_type);
        name = (TextView) view.findViewById(R.id.short_title);
        date = (TextView) view.findViewById(R.id.short_date);
        genre = (TextView) view.findViewById(R.id.short_genre);
        score = (TextView) view.findViewById(R.id.short_score);
    }
}
