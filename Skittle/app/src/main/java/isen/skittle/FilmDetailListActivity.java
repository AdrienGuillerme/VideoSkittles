package isen.skittle;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import isen.skittle.api.Movie;
import isen.skittle.async.GetBackground;
import isen.skittle.async.GetPoster;
import isen.skittle.database.SkittleDatabaseContract;
import isen.skittle.database.SkittleDatabaseManager;
import isen.skittle.utils.Constants;

/**
 * Created by adrie on 08/03/2017.
 */

public class FilmDetailListActivity extends AppCompatActivity implements View.OnClickListener {

    private Movie dbmovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.film_details_layout);
        findViewById(R.id.movie_del_button).setOnClickListener(this);
        findViewById(R.id.movie_del_section).setVisibility(View.VISIBLE);
        findViewById(R.id.movie_add_section).setVisibility(View.INVISIBLE);

        int id = 0;
        // Retrieve the id passed as parameter
        final Intent intent = getIntent();
        if (null != intent) {
            final Bundle extras = intent.getExtras();
            if ((null != extras) && (extras.containsKey(Constants.General.ID))) {
                // Retrieve the id
                id = extras.getInt(Constants.General.ID);
                // Set as ActionBar subtitle
                getSupportActionBar().setSubtitle("Movie ID : " + id);
                String[] arg = new String[1];
                arg[0] = String.valueOf(id);
                Cursor cursor = SkittleApplication.getContext().getContentResolver().query(SkittleDatabaseContract.MOVIES_URI, SkittleDatabaseContract.PROJECTION_FULL_MOVIES, SkittleDatabaseContract.SELECTION_BY_ID, arg, null);
                onMovieQuery(cursor);
            }
        }
    }

    public void onMovieQuery(Cursor cursor) {
        SkittleDatabaseManager dbManager = new SkittleDatabaseManager();
        Movie movie;
        int rows = cursor.getCount();
        if (rows > 0) {
            cursor.moveToNext();
            movie = dbManager.movieFromCursor(cursor);
            dbmovie = movie;
            ((TextView) findViewById(R.id.movie_title)).setText(movie.title);
            ((TextView) findViewById(R.id.movie_actors)).setText(movie.actors);
            ((TextView) findViewById(R.id.movie_date)).setText(movie.airDate);
            ((TextView) findViewById(R.id.movie_overview)).setText(movie.overview);
            ((TextView) findViewById(R.id.movie_country)).setText(movie.countriesIso);
            ((TextView) findViewById(R.id.movie_runtime)).setText(String.valueOf(movie.runtime) + "min");
            ((TextView) findViewById(R.id.movie_genre)).setText(movie.genresString);
            ((TextView) findViewById(R.id.movie_director)).setText(movie.directors);
            ((TextView) findViewById(R.id.movie_rating)).setText("Rating : "+String.valueOf(movie.rating) + "/10");
            GetPoster getPoster = new GetPoster((ImageView) findViewById(R.id.movie_poster));
            getPoster.execute(movie.posterPath);
            GetBackground getBackground = new GetBackground((ImageView) findViewById(R.id.movie_backdrop));
            getBackground.execute(movie.bgPath);
        }else {
            Log.i(Constants.General.LOG_TAG,"Empty query");
        }

    }


    @Override
    public void onClick(View v) {
        SkittleDatabaseManager dbManager = new SkittleDatabaseManager();

        //SkittleApplication.getContext().getContentResolver().delete(SkittleDatabaseContract.MOVIES_URI, );
        Toast.makeText(this, R.string.del_movie_from_list, Toast.LENGTH_LONG).show();
    }
}
