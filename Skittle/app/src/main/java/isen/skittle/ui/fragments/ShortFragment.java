package isen.skittle.ui.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isen.skittle.FilmDetailActivity;
import isen.skittle.R;
import isen.skittle.SeriesDetailActivity;
import isen.skittle.adapters.ShortAdapter;
import isen.skittle.api.Results;
import isen.skittle.api.Short;
import isen.skittle.async.GetPoster;
import isen.skittle.async.SearchTMDB;
import isen.skittle.holder.ShortViewHolder;
import isen.skittle.interfaces.PosterRequestListener;
import isen.skittle.interfaces.ShortRequestListener;
import isen.skittle.utils.Constants;

/**
 * Created by adrie on 07/03/2017.
 */

public class ShortFragment extends Fragment implements ShortRequestListener, AdapterView.OnItemClickListener {

    private ListView view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        // Get the ListView
        view = (ListView) rootView.findViewById(R.id.searchListView);
        String query = getArguments().getString(Constants.General.SEARCH);
        SearchTMDB search = new SearchTMDB(this);
        search.execute(Constants.Url.baseMultiURL + query.replace(' ', '+'));
        return rootView;
    }

    @Override
    public void onShortRequestAnswer(Results results) {
        List<Short> shorts = new ArrayList<Short>();
        for(Short shortRes : results.results){
            shortRes.setGenresString();
            shorts.add(shortRes);
        }
        final ShortAdapter adapter = new ShortAdapter(shorts);
        view.setAdapter(adapter);
        view.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("CLICK", "onItemClick: ");
        int id = ((ShortViewHolder) view.getTag()).id;
        String type = (((ShortViewHolder) view.getTag()).type).getText().toString();
        final Intent intent;
        if(type.equals("tv")){
            intent = new Intent(this.getActivity().getApplicationContext(), SeriesDetailActivity.class);
        }
        else {
            intent = new Intent(this.getActivity().getApplicationContext(), FilmDetailActivity.class);
        }
        final Bundle extras = new Bundle();
        extras.putInt(Constants.General.ID, id);
        intent.putExtras(extras);
        startActivity(intent);
    }
}
