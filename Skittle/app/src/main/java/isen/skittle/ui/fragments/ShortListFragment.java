package isen.skittle.ui.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import isen.skittle.FilmDetailActivity;
import isen.skittle.R;
import isen.skittle.SeriesDetailActivity;
import isen.skittle.SkittleApplication;
import isen.skittle.adapters.MovieAdapter;
import isen.skittle.api.Movie;
import isen.skittle.database.SkittleDatabaseContract;
import isen.skittle.database.SkittleDatabaseManager;
import isen.skittle.holder.MovieViewHolder;
import isen.skittle.holder.ShortViewHolder;
import isen.skittle.utils.Constants;

/**
 * Created by adrie on 08/03/2017.
 */

public class ShortListFragment extends Fragment implements AdapterView.OnItemClickListener{
    private ListView view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        //SkittleDatabaseProvider dbProvider = new SkittleDatabaseProvider();
        SkittleDatabaseManager dbManager = new SkittleDatabaseManager();

        // Get the ListView
        view = (ListView) rootView.findViewById(R.id.searchListView);

        Cursor cursor = SkittleApplication.getContext().getContentResolver().query(SkittleDatabaseContract.MOVIES_URI, SkittleDatabaseContract.PROJECTION_FULL_MOVIES, null, null, null);
        List<Movie> movies = new ArrayList<>();
        int rows = cursor.getCount();
        if (rows > 0) {
            for (int i = 0; i < rows; i++) {
                cursor.moveToNext();
                movies.add(dbManager.movieFromCursor(cursor));
            }
        }else {
            Log.i(Constants.General.LOG_TAG,"Empty List");
        }
        final MovieAdapter adapter = new MovieAdapter(movies);
        view.setAdapter(adapter);
        view.setOnItemClickListener(this);

        return rootView;
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Log.i("CLICK", "onItemClick: ");
        int id = ((MovieViewHolder) view.getTag()).id;
        Log.i(Constants.General.LOG_TAG, "id " + id);
        final Intent intent;
        intent = new Intent(this.getActivity().getApplicationContext(), FilmDetailActivity.class);

        final Bundle extras = new Bundle();
        extras.putInt(Constants.General.ID, id);
        intent.putExtras(extras);
        startActivity(intent);
    }
}
