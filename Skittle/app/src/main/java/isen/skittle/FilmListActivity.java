package isen.skittle;


import android.app.FragmentTransaction;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import isen.skittle.api.Movie;
import isen.skittle.database.SkittleDatabaseManager;
import isen.skittle.ui.fragments.ShortListFragment;
import android.support.v7.app.AppCompatActivity;


public class FilmListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        getSupportActionBar().setSubtitle("My movies");

        if (savedInstanceState == null) {
            final FragmentTransaction transaction = getFragmentManager().beginTransaction();
            final ShortListFragment fragment = new ShortListFragment();;
            transaction.add(R.id.activity_list, fragment).commit();
        }

    }

}