package isen.skittle.api;

import com.google.gson.annotations.SerializedName;

import isen.skittle.utils.Constants;

public class Short {

    @SerializedName("poster_path")
    public String posterPath;

    @SerializedName("vote_average")
    public double rating;

    @SerializedName("id")
    public int id;

    @SerializedName("media_type")
    public String mediaType;

    @SerializedName("first_air_date")
    public String airDate;

    @SerializedName("release_date")
    public String releaseDate;

    @SerializedName("genre_ids")
    public int[] genreIds;

    @SerializedName("name")
    public String name;

    @SerializedName("title")
    public String title;

    public String genres = "";

    public void setGenresString() {
        if(genreIds != null && genreIds.length>0){
            int genreIndex;
            int i;
            for (i = 0; i < genreIds.length - 1; i++) {
                genreIndex = Constants.Genres.genreList.indexOf(String.valueOf(genreIds[i]));
                genres += Constants.Genres.genreList.get(genreIndex + 1) + ", ";
            }
            genreIndex = Constants.Genres.genreList.indexOf(String.valueOf(genreIds[i]));
            genres += Constants.Genres.genreList.get(genreIndex + 1);
        }
        else{
            genres = "Unknown";
        }
    }

}
