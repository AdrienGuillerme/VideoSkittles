package isen.skittle.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by docte on 04/03/2017.
 */

public class Results {
    @SerializedName("results")
    public Short[] results;

    @SerializedName("total_results")
    public int nbResult;

}
