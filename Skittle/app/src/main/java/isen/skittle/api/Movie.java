package isen.skittle.api;

import com.google.gson.annotations.SerializedName;

import isen.skittle.utils.Constants;

/**
 * Created by docte on 04/03/2017.
 */

public class Movie {
    @SerializedName("poster_path")
    public String posterPath;

    @SerializedName("backdrop_path")
    public String bgPath;

    @SerializedName("vote_average")
    public double rating;

    @SerializedName("id")
    public int id;

    public String mediaType = "Movie";

    @SerializedName("release_date")
    public String airDate;

    @SerializedName("title")
    public String title;

    @SerializedName("production_countries")
    public Country[] countriesTab;

    public String countriesIso ="";

    public void setCountriesToString(){
        int i;
        for(i = 0; i< countriesTab.length-1; i++){
            countriesIso += countriesTab[i].iso + ", ";
        }
        countriesIso += countriesTab[i].iso;
    }

    @SerializedName("runtime")
    public int runtime;

    @SerializedName("overview")
    public String overview;

    @SerializedName("genres")
    public Genre[] genresTab;

    public String genresString = "";

    public void setGenresString() {
        int i;
        for (i = 0; i < genresTab.length - 1; i++) {
            genresString += genresTab[i].name + ", ";
        }
        genresString += genresTab[i].name;
    }

    public String actors;

    public String directors;

    public void setActor(String newActors) {
        this.actors = newActors;
    }

    public void setDirectors(String newDirectors) {
        this.directors = newDirectors;
    }

    @Override
    public String toString() {
        return title;
    }
}
