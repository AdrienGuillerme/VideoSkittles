package isen.skittle.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by docte on 04/03/2017.
 */

public class Serie {

    @SerializedName("poster_path")
    public String posterPath;

    @SerializedName("backdrop_path")
    public String bgPath;

    @SerializedName("vote_average")
    public double rating;

    @SerializedName("id")
    public int id;

    public String mediaType = "TV";

    @SerializedName("first_air_date")
    public String airDate;

    @SerializedName("name")
    public String name;

    @SerializedName("episode_run_time")
    public int[] lengths;

    @SerializedName("number_of_episodes")
    public int nbEpisodes;

    @SerializedName("number_of_seasons")
    public int nbSeasons;

    @SerializedName("origin_country")
    public String[] countriesTab;

    public String countriesIso = "";

    public void setCountriesToString(){
        int i;
        for(i = 0; i< countriesTab.length - 1; i++){
            countriesIso += countriesTab[i] + ", ";
        }
        countriesIso += countriesTab[i];
    }

    @SerializedName("overview")
    public String overview;

    @SerializedName("genres")
    public Genre[] genresTab;

    public String genresString = "";

    public void setGenresString() {
        int i;
        for (i = 0; i < genresTab.length - 1; i++) {
            genresString += genresTab[i].name + ", ";
        }
        genresString += genresTab[i].name;
    }

    public String actors;

    public String writers;

    public void setActor(String newActors) {
        this.actors = newActors;
    }

    public void setWriter(String newWrites) {
        this.writers = newWrites;
    }
}
