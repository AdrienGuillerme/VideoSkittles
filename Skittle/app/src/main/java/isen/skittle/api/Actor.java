package isen.skittle.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by docte on 05/03/2017.
 */

public class Actor {
    @SerializedName("Actors")
    public String actorList;

    @SerializedName("Director")
    public String directorList;

    @SerializedName("Writer")
    public String writerList;
}
