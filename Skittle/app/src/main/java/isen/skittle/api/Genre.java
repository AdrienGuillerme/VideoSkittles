package isen.skittle.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by docte on 05/03/2017.
 */

public class Genre {
    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;
}
