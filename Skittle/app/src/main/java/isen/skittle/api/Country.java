package isen.skittle.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by docte on 05/03/2017.
 */

public class Country {
    @SerializedName("iso_3166_1")
    public String iso;

    @SerializedName("name")
    public String name;
}
