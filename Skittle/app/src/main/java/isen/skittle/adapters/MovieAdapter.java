package isen.skittle.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import isen.skittle.R;
import isen.skittle.SkittleApplication;
import isen.skittle.api.Movie;
import isen.skittle.api.Short;
import isen.skittle.async.GetPoster;
import isen.skittle.holder.MovieViewHolder;

/**
 * Created by adrie on 08/03/2017.
 */

public class MovieAdapter extends BaseAdapter {

    private final List<Movie> mMovies;

    private final LayoutInflater mInflater;

    public MovieAdapter (List<Movie> movies){
        mMovies = movies;
        mInflater = LayoutInflater.from(SkittleApplication.getContext());
    }

    @Override
    public int getCount() {
        return null != mMovies ? mMovies.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return null != mMovies ? mMovies.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MovieViewHolder holder;
        // If we don't have any convertView to reuse, inflate one
        if (null == convertView){
            convertView = mInflater.inflate(R.layout.short_item, null);

            // Instantiate the ViewHolder
            holder = new MovieViewHolder(convertView);
            // Set as tag to the convertView to retrieve it easily
            convertView.setTag(holder);
        } else {
            // Just retrieve the ViewHolder instance in the tag of the view
            holder = (MovieViewHolder) convertView.getTag();
        }

        // Get the current item
        final Movie movieRes = (Movie) getItem(position);

        // Set the info

        holder.name.setText(movieRes.title);

        holder.type.setText(movieRes.mediaType);
        holder.genre.setText(movieRes.genresString);
        holder.score.setText(String.valueOf(movieRes.rating));
        holder.date.setText(movieRes.airDate);
        holder.id = movieRes.id;

//        // Display the images --> use cache later to improve performances
//        final Bitmap image = mImageMemoryCache.getBitmapFromMemCache(tweet.user.profileImageUrl);
//        if (null == image){
//            new DownloadImageAsyncTask(holder.image, mImageMemoryCache).execute(tweet.user.profileImageUrl);
//        } else {
//            holder.image.setImageBitmap(image);
//        }

        GetPoster getPoster = new GetPoster(holder.coverImage);
        getPoster.execute(movieRes.posterPath);


        //add on click to button

        return convertView;
    }
}
