package isen.skittle.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import isen.skittle.R;
import isen.skittle.SkittleApplication;
import isen.skittle.api.Short;
import isen.skittle.async.GetPoster;
import isen.skittle.holder.ShortViewHolder;

/**
 * Created by docte on 08/03/2017.
 */

public class ShortAdapter extends BaseAdapter{

    private final List<Short> mShorts;

    private final LayoutInflater mInflater;

    public ShortAdapter (List<Short> shorts){
        mShorts = shorts;
        mInflater = LayoutInflater.from(SkittleApplication.getContext());
    }

    @Override
    public int getCount() {
        return null != mShorts ? mShorts.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return null != mShorts ? mShorts.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ShortViewHolder holder;
        // If we don't have any convertView to reuse, inflate one
        if (null == convertView){
            convertView = mInflater.inflate(R.layout.short_item, null);

            // Instantiate the ViewHolder
            holder = new ShortViewHolder(convertView);
            // Set as tag to the convertView to retrieve it easily
            convertView.setTag(holder);
        } else {
            // Just retrieve the ViewHolder instance in the tag of the view
            holder = (ShortViewHolder) convertView.getTag();
        }

        // Get the current item
        final Short shortRes = (Short) getItem(position);

        // Set the info
        if(shortRes.mediaType.equals("tv")){
            holder.name.setText(shortRes.name);
            holder.date.setText(shortRes.airDate);
        } else {
            holder.name.setText(shortRes.title);
            holder.date.setText(shortRes.releaseDate);
        }
        holder.type.setText(shortRes.mediaType);
        holder.genre.setText(shortRes.genres);
        holder.score.setText(String.valueOf(shortRes.rating));
        holder.id = shortRes.id;


//        // Display the images --> use cache later to improve performances
//        final Bitmap image = mImageMemoryCache.getBitmapFromMemCache(tweet.user.profileImageUrl);
//        if (null == image){
//            new DownloadImageAsyncTask(holder.image, mImageMemoryCache).execute(tweet.user.profileImageUrl);
//        } else {
//            holder.image.setImageBitmap(image);
//        }

        GetPoster getPoster = new GetPoster(holder.coverImage);
        getPoster.execute(shortRes.posterPath);


        //TODO add on click to button

        return convertView;
    }

}