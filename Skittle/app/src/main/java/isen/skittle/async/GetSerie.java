package isen.skittle.async;

import android.os.AsyncTask;
import android.util.Log;

import isen.skittle.api.Serie;
import isen.skittle.helper.TMDBHelper;
import isen.skittle.interfaces.SerieRequestListener;


public class GetSerie extends AsyncTask<Integer, Void, Serie> {

    private SerieRequestListener mListener;

    public GetSerie(SerieRequestListener listener) {
        mListener = listener;
    }

    @Override
    protected Serie doInBackground(Integer... ints) {
        Log.i("TMDB", "SerieRequest : " + ints[0]);
        try {
            Serie serie = TMDBHelper.getSerieDetails(ints[0]);
            return serie;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Serie serie) {
        if (null != mListener && null != serie) {
            mListener.onSerieRequestAnswer(serie);
        }
    }
}
