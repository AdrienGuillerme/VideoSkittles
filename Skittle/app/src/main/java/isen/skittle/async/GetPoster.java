package isen.skittle.async;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.ImageView;

import isen.skittle.helper.TMDBHelper;
import isen.skittle.interfaces.PosterRequestListener;


public class GetPoster extends AsyncTask<String, Void, Bitmap> {

    private ImageView mImageView;

    public GetPoster(ImageView imageView) {
        mImageView = imageView;
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        Log.i("TMDB", "PosterRequest : " + strings[0]);
        try {
            return TMDBHelper.getPoster(strings[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (null != mImageView && null != bitmap) {
            mImageView.setImageBitmap(bitmap);
        }
    }
}
