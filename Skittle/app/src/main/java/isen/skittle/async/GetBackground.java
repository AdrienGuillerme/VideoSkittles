package isen.skittle.async;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import isen.skittle.helper.TMDBHelper;
import isen.skittle.interfaces.BgRequestListener;


public class GetBackground extends AsyncTask<String, Void, Bitmap> {

    private ImageView mImageView;

    public GetBackground(ImageView imageView) {
        mImageView = imageView;
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        Log.i("TMDB", "BackgroundRequest : " + strings[0]);
        try {
            return TMDBHelper.getBackground(strings[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (null != mImageView && null != bitmap) {
            mImageView.setImageBitmap(bitmap);
        }
    }
}
