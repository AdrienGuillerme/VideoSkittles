package isen.skittle.async;

import android.os.AsyncTask;
import android.util.Log;

import isen.skittle.api.Movie;
import isen.skittle.helper.TMDBHelper;
import isen.skittle.interfaces.MovieRequestListener;


public class GetMovie extends AsyncTask<Integer, Void, Movie> {

    private MovieRequestListener mListener;

    public GetMovie(MovieRequestListener listener) {
        mListener = listener;
    }

    @Override
    protected Movie doInBackground(Integer... ints) {
        Log.i("TMDB", "MovieRequest: " + ints[0]);
        try {
            Movie movie = TMDBHelper.getMovieDetails(ints[0]);
            return movie;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Movie movie) {
        if (null != mListener && null != movie) {
            mListener.onMovieRequestAnswer(movie);
        }
    }
}
