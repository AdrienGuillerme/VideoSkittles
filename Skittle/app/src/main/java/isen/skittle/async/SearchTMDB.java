package isen.skittle.async;

import android.os.AsyncTask;
import android.util.Log;

import isen.skittle.api.Results;
import isen.skittle.helper.TMDBHelper;
import isen.skittle.interfaces.ShortRequestListener;


public class SearchTMDB extends AsyncTask<String, Void, Results> {

    private ShortRequestListener mListener;

    public SearchTMDB(ShortRequestListener listener) {
        mListener = listener;
    }

    @Override
    protected Results doInBackground(String... strings) {
        Log.i("TMDB", "SearchRequest: " + strings[0]);
        try {
            return TMDBHelper.getShortType(strings[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Results results) {
        if (null != mListener && null != results) {
            mListener.onShortRequestAnswer(results);
        }
    }
}
