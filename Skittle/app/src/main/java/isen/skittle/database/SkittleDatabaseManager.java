package isen.skittle.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import isen.skittle.SkittleApplication;
import isen.skittle.api.Movie;
import isen.skittle.api.Serie;
import isen.skittle.utils.Constants;

/**
 * Created by adrie on 05/03/2017.
 */

public class SkittleDatabaseManager {

    public static Movie movieFromCursor(Cursor c){
        if (null != c){
            final Movie movie = new Movie();

            if (c.getColumnIndex(SkittleDatabaseContract.ID) >= 0){
                movie.id = c.getInt(c.getColumnIndex(SkittleDatabaseContract.ID));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.TITLE) >= 0){
                movie.title = c.getString(c.getColumnIndex(SkittleDatabaseContract.TITLE));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.POSTER) >= 0){
                movie.posterPath = c.getString(c.getColumnIndex(SkittleDatabaseContract.POSTER));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.BACKDROP) >= 0){
                movie.bgPath = c.getString(c.getColumnIndex(SkittleDatabaseContract.BACKDROP));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.OVERVIEW) >= 0){
                movie.overview = c.getString(c.getColumnIndex(SkittleDatabaseContract.OVERVIEW));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.RATING) >= 0){
                movie.rating = c.getDouble(c.getColumnIndex(SkittleDatabaseContract.RATING));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.COUNTRIES) >= 0){
                movie.countriesIso = c.getString(c.getColumnIndex(SkittleDatabaseContract.COUNTRIES));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.AIRDATE) >= 0){
                movie.airDate = c.getString(c.getColumnIndex(SkittleDatabaseContract.AIRDATE));
            }

            if (c.getColumnIndex(SkittleDatabaseContract.RUNTIME) >= 0){
                movie.runtime = c.getInt(c.getColumnIndex(SkittleDatabaseContract.RUNTIME));

            }
            if (c.getColumnIndex(SkittleDatabaseContract.GENRES) >= 0){
                movie.genresString = c.getString(c.getColumnIndex(SkittleDatabaseContract.GENRES));
            }

            if (c.getColumnIndex(SkittleDatabaseContract.ACTORS) >= 0){
                movie.actors = c.getString(c.getColumnIndex(SkittleDatabaseContract.ACTORS));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.DIRECTOR) >= 0){
                movie.directors = c.getString(c.getColumnIndex(SkittleDatabaseContract.DIRECTOR));
            }

            return movie;
        }
        return null;
    }

    public static Serie SerieFromCursor(Cursor c){
        if (null != c){
            final Serie serie = new Serie();

            if (c.getColumnIndex(SkittleDatabaseContract.ID) >= 0){
                serie.id = c.getInt(c.getColumnIndex(SkittleDatabaseContract.ID));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.NAME) >= 0){
                serie.name = c.getString(c.getColumnIndex(SkittleDatabaseContract.NAME));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.POSTER) >= 0){
                serie.posterPath = c.getString(c.getColumnIndex(SkittleDatabaseContract.POSTER));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.BACKDROP) >= 0){
                serie.bgPath = c.getString(c.getColumnIndex(SkittleDatabaseContract.BACKDROP));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.OVERVIEW) >= 0){
                serie.overview = c.getString(c.getColumnIndex(SkittleDatabaseContract.OVERVIEW));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.RATING) >= 0){
                serie.rating = c.getDouble(c.getColumnIndex(SkittleDatabaseContract.RATING));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.COUNTRIES) >= 0){
                serie.countriesIso = c.getString(c.getColumnIndex(SkittleDatabaseContract.COUNTRIES));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.AIRDATE) >= 0){
                serie.airDate = c.getString(c.getColumnIndex(SkittleDatabaseContract.AIRDATE));
            }
            //if (c.getColumnIndex(SkittleDatabaseContract.EPISODERUNTIME) >= 0){
            //   serie.lengths = c.get(c.getColumnIndex(SkittleDatabaseContract.EPISODERUNTIME));
            //}
            if (c.getColumnIndex(SkittleDatabaseContract.NBSEASONS) >= 0){
                serie.nbSeasons = c.getInt(c.getColumnIndex(SkittleDatabaseContract.NBSEASONS));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.NBEPISODES) >= 0){
                serie.nbEpisodes = c.getInt(c.getColumnIndex(SkittleDatabaseContract.NBEPISODES));
            }

            if (c.getColumnIndex(SkittleDatabaseContract.ACTORS) >= 0){
                serie.actors = c.getString(c.getColumnIndex(SkittleDatabaseContract.ACTORS));
            }
            if (c.getColumnIndex(SkittleDatabaseContract.WRITER) >= 0){
                serie.writers = c.getString(c.getColumnIndex(SkittleDatabaseContract.WRITER));
            }

            return serie;
        }
        return null;
    }

    public static ContentValues movieToContentValues(Movie movie){
        final ContentValues values = new ContentValues();

        if ((movie.id) != -1){
            values.put(SkittleDatabaseContract.ID, movie.id);
        }
        if (!TextUtils.isEmpty(movie.title)){
            values.put(SkittleDatabaseContract.TITLE, movie.title);
        }
        if (!TextUtils.isEmpty(movie.posterPath)){
            values.put(SkittleDatabaseContract.POSTER, movie.posterPath);
        }
        if (!TextUtils.isEmpty(movie.bgPath)){
            values.put(SkittleDatabaseContract.BACKDROP, movie.bgPath);
        }
        if (!TextUtils.isEmpty(movie.overview)){
            values.put(SkittleDatabaseContract.OVERVIEW, movie.overview);
        }
        if (!TextUtils.isEmpty(movie.airDate)){
            values.put(SkittleDatabaseContract.AIRDATE, movie.airDate);
        }
        if (!TextUtils.isEmpty(movie.countriesIso)){
            values.put(SkittleDatabaseContract.COUNTRIES, movie.countriesIso);
        }

        if (!TextUtils.isEmpty(movie.genresString)){
            values.put(SkittleDatabaseContract.GENRES, movie.genresString);
        }
        if (movie.rating != -1){
            values.put(SkittleDatabaseContract.RATING, movie.rating);
        }
        if (!TextUtils.isEmpty(movie.actors)){
            values.put(SkittleDatabaseContract.ACTORS, movie.actors);
        }
        if (!TextUtils.isEmpty(movie.directors)){
            values.put(SkittleDatabaseContract.DIRECTOR, movie.directors);
        }

        return values;
    }

    public static ContentValues serieToContentValues(Serie serie){
        final ContentValues values = new ContentValues();

        if ((serie.id) != 0){
            values.put(SkittleDatabaseContract.ID, serie.id);
        }
        if (!TextUtils.isEmpty(serie.name)){
            values.put(SkittleDatabaseContract.NAME, serie.name);
        }
        if (!TextUtils.isEmpty(serie.posterPath)){
            values.put(SkittleDatabaseContract.POSTER, serie.posterPath);
        }
        if (!TextUtils.isEmpty(serie.bgPath)){
            values.put(SkittleDatabaseContract.BACKDROP, serie.bgPath);
        }
        if (!TextUtils.isEmpty(serie.overview)){
            values.put(SkittleDatabaseContract.OVERVIEW, serie.overview);
        }
        if (!TextUtils.isEmpty(serie.airDate)){
            values.put(SkittleDatabaseContract.AIRDATE, serie.airDate);
        }
        if (!TextUtils.isEmpty(serie.countriesIso)){
            values.put(SkittleDatabaseContract.COUNTRIES, serie.countriesIso);
        }
        if (serie.rating != 0){
            values.put(SkittleDatabaseContract.RATING, serie.rating);
        }
        //if (serie.lengths != null){
        //    values.put(SkittleDatabaseContract.RUNTIME, serie.lengths);
        //}
        if (serie.nbEpisodes != 0){
            values.put(SkittleDatabaseContract.NBEPISODES, serie.nbEpisodes);
        }
        if (serie.nbSeasons != 0){
            values.put(SkittleDatabaseContract.NBSEASONS, serie.nbSeasons);
        }
        if (!TextUtils.isEmpty(serie.genresString)){
            values.put(SkittleDatabaseContract.GENRES, serie.genresString);
        }
        if (!TextUtils.isEmpty(serie.actors)){
            values.put(SkittleDatabaseContract.ACTORS, serie.actors);
        }
        if (!TextUtils.isEmpty(serie.writers)){
            values.put(SkittleDatabaseContract.WRITER, serie.writers);
        }

        return values;
    }


    public static synchronized int insertMovie(Movie movie){
        if (null != movie){
            if (!doesContainMovie(movie)){
                final Uri uri = SkittleApplication.getContext().getContentResolver().insert(
                        SkittleDatabaseContract.MOVIES_URI, movieToContentValues(movie));
                if (null != uri){
                    return Integer.parseInt(uri.getLastPathSegment());
                } else {
                    return -1;
                }
            }
        }
        return -1;
    }

    public static synchronized int insertSerie(Serie serie){
        if (null != serie){
            if (!doesContainSerie(serie)){
                final Uri uri = SkittleApplication.getContext().getContentResolver().insert(
                        SkittleDatabaseContract.SERIES_URI, serieToContentValues(serie));
                if (null != uri){
                    return Integer.parseInt(uri.getLastPathSegment());
                } else {
                    return -1;
                }
            }
        }
        return -1;
    }

    public static synchronized List<Movie> getStoredMovies(){
        final List<Movie> movies = new ArrayList<Movie>();
        final Cursor cursor = SkittleApplication.getContext().getContentResolver().query(
                SkittleDatabaseContract.MOVIES_URI, SkittleDatabaseContract.PROJECTION_FULL_MOVIES, null, null, null);
        if (null != cursor){
            while (cursor.moveToNext()){
                movies.add(movieFromCursor(cursor));
            }
        }
        if ((null != cursor) && (!cursor.isClosed())) {
            cursor.close();
        }
        return movies;
    }

    public static synchronized List<Movie> getStoredSeries(){
        final List<Movie> series = new ArrayList<Movie>();
        final Cursor cursor = SkittleApplication.getContext().getContentResolver().query(
                SkittleDatabaseContract.SERIES_URI, SkittleDatabaseContract.PROJECTION_FULL_SERIES, null, null, null);
        if (null != cursor){
            while (cursor.moveToNext()){
                series.add(movieFromCursor(cursor));
            }
        }
        if ((null != cursor) && (!cursor.isClosed())) {
            cursor.close();
        }
        return series;
    }

    public static synchronized void dropMovieDatabase(){
        SkittleApplication.getContext().getContentResolver().delete(
                SkittleDatabaseContract.MOVIES_URI, null, null);
    }

    public static synchronized void dropSerieDatabase(){
        SkittleApplication.getContext().getContentResolver().delete(
                SkittleDatabaseContract.SERIES_URI, null, null);
    }

    public static synchronized boolean doesContainMovie(Movie movie){
        boolean result = false;
        if ((null != movie) && (movie.id != 0)){
            final Cursor cursor = SkittleApplication.getContext().getContentResolver().query(
                    SkittleDatabaseContract.MOVIES_URI, SkittleDatabaseContract.PROJECTION_FULL_MOVIES,
                    SkittleDatabaseContract.SELECTION_BY_TMDBID, new String[]{String.valueOf(movie.id)}, null);
            //check string declaration here         ->                ^
            if ((null != cursor) && (cursor.moveToFirst())) {
                result = true;
            }
            if ((null != cursor) && (!cursor.isClosed())) {
                cursor.close();
            }
        }
        return result;
    }

    public static synchronized boolean doesContainSerie(Serie serie){
        boolean result = false;
        if ((null != serie) && (serie.id != 0)){
            final Cursor cursor = SkittleApplication.getContext().getContentResolver().query(
                    SkittleDatabaseContract.SERIES_URI, SkittleDatabaseContract.PROJECTION_FULL_SERIES,
                    SkittleDatabaseContract.SELECTION_BY_TMDBID, new String[]{String.valueOf(serie.id)}, null);
            //check string declaration here         ->                ^
            if ((null != cursor) && (cursor.moveToFirst())) {
                result = true;
            }
            if ((null != cursor) && (!cursor.isClosed())) {
                cursor.close();
            }
        }
        return result;
    }

    public static void testMovieDatabase(List<Movie> movies){
        final SkittleDatabaseHelper dbHelper = new SkittleDatabaseHelper(SkittleApplication.getContext());
        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        // First insert all values in database
        for (Movie movie : movies){
            db.insert(SkittleDatabaseContract.TABLE_MOVIES, "", movieToContentValues(movie));
            Log.w(Constants.General.LOG_TAG, "Movie stored");
            Log.w(Constants.General.LOG_TAG, movies.toString());
            Log.w(Constants.General.LOG_TAG, "----------------------");
        }

        // Now that all values are stored in database, read them and log
        final Cursor cursor = db.query(SkittleDatabaseContract.TABLE_MOVIES,
                SkittleDatabaseContract.PROJECTION_FULL_MOVIES,
                null, null, null, null, null);
        if (null != cursor){
            while (cursor.moveToNext()){
                final Movie movie = movieFromCursor(cursor);
                Log.i(Constants.General.LOG_TAG, "Movie stored");
                Log.i(Constants.General.LOG_TAG, movies.toString());
                Log.i(Constants.General.LOG_TAG, "----------------------");
            }
        }
    }

    public static void testContentProvider(){
        // Test the query
        SkittleApplication.getContext().getContentResolver().query(
                SkittleDatabaseContract.MOVIES_URI, SkittleDatabaseContract.PROJECTION_FULL_MOVIES,
                null, null, null);

        // Test the insert
        SkittleApplication.getContext().getContentResolver().insert(
                SkittleDatabaseContract.MOVIES_URI, null);

        // Test the update
        SkittleApplication.getContext().getContentResolver().update(
                SkittleDatabaseContract.MOVIES_URI, null, null, null);

        // Test the delete
        SkittleApplication.getContext().getContentResolver().delete(
                SkittleDatabaseContract.MOVIES_URI, null, null);
    }
}