package isen.skittle.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by adrie on 08/03/2017.
 */

public class SkittleDatabaseProvider extends ContentProvider {

    // Our database helper
    private SkittleDatabaseHelper mDBHelper;

    // The URI matcher to check if the URI is correct
    private UriMatcher mUriMatcher;

    // The URI matcher code for correct result
    private static final int MOVIE_CORRECT_URI_CODE = 42;
    private static final int SERIE_CORRECT_URI_CODE = 43;

    // Use a Lock to be thread-safe
    private final ReentrantLock mLock = new ReentrantLock();

    @Override
    public boolean onCreate() {
        mDBHelper = new SkittleDatabaseHelper(getContext());
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(SkittleDatabaseContract.CONTENT_PROVIDER_MOVIES_AUTHORITY,
                SkittleDatabaseContract.TABLE_MOVIES, MOVIE_CORRECT_URI_CODE);
        mUriMatcher.addURI(SkittleDatabaseContract.CONTENT_PROVIDER_MOVIES_AUTHORITY,
                SkittleDatabaseContract.TABLE_SERIES, SERIE_CORRECT_URI_CODE);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (projection == SkittleDatabaseContract.PROJECTION_FULL_MOVIES){
            mLock.lock();
            try {
                SQLiteDatabase db = mDBHelper.getReadableDatabase();
                Cursor c = db.query(SkittleDatabaseContract.TABLE_MOVIES, projection, selection, selectionArgs, null, null, sortOrder);
                c.setNotificationUri(getContext().getContentResolver(), uri);
                return c;
            } catch (Exception e){
                return null;
            } finally {
                mLock.unlock();
            }
        }
        if (projection == SkittleDatabaseContract.PROJECTION_FULL_SERIES){
            mLock.lock();
            try {
                SQLiteDatabase db = mDBHelper.getReadableDatabase();
                Cursor c = db.query(SkittleDatabaseContract.TABLE_SERIES, projection, selection, selectionArgs, null, null, sortOrder);
                c.setNotificationUri(getContext().getContentResolver(), uri);
                return c;
            } catch (Exception e){
                return null;
            } finally {
                mLock.unlock();
            }
        }
        return null;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        if (mUriMatcher.match(uri) == MOVIE_CORRECT_URI_CODE){
            return SkittleDatabaseContract.MOVIES_CONTENT_TYPE;
        }
        if (mUriMatcher.match(uri) == SERIE_CORRECT_URI_CODE){
            return SkittleDatabaseContract.SERIES_CONTENT_TYPE;
        }
        throw new IllegalArgumentException("Unknown URI " + uri);
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if ((mUriMatcher.match(uri) == MOVIE_CORRECT_URI_CODE) && (null != values)){
            mLock.lock();
            try {
                final SQLiteDatabase db = mDBHelper.getWritableDatabase();
                final long rowId = db.insert(SkittleDatabaseContract.TABLE_MOVIES, "", values);
                if (rowId > 0) {
                    final Uri applicationUri = ContentUris.withAppendedId(SkittleDatabaseContract.MOVIES_URI, rowId);
                    getContext().getContentResolver().notifyChange(applicationUri, null);
                    return applicationUri;
                }
            } catch (Exception e){
                return null;
            } finally {
                mLock.unlock();
            }
        }
        if ((mUriMatcher.match(uri) == SERIE_CORRECT_URI_CODE) && (null != values)){
            mLock.lock();
            try {
                final SQLiteDatabase db = mDBHelper.getWritableDatabase();
                final long rowId = db.insert(SkittleDatabaseContract.TABLE_SERIES, "", values);
                if (rowId > 0) {
                    final Uri applicationUri = ContentUris.withAppendedId(SkittleDatabaseContract.SERIES_URI, rowId);
                    getContext().getContentResolver().notifyChange(applicationUri, null);
                    return applicationUri;
                }
            } catch (Exception e){
                return null;
            } finally {
                mLock.unlock();
            }
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        if (mUriMatcher.match(uri) == MOVIE_CORRECT_URI_CODE){
            mLock.lock();
            try {
                final SQLiteDatabase db = mDBHelper.getWritableDatabase();
                final int count = db.delete(SkittleDatabaseContract.TABLE_MOVIES, where, whereArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                return count;
            } catch (Exception e){
                return 0;
            } finally {
                mLock.unlock();
            }
        }
        if (mUriMatcher.match(uri) == SERIE_CORRECT_URI_CODE){
            mLock.lock();
            try {
                final SQLiteDatabase db = mDBHelper.getWritableDatabase();
                final int count = db.delete(SkittleDatabaseContract.TABLE_SERIES, where, whereArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                return count;
            } catch (Exception e){
                return 0;
            } finally {
                mLock.unlock();
            }
        }
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if ((mUriMatcher.match(uri) == MOVIE_CORRECT_URI_CODE) && (null != values)){
            mLock.lock();
            try {
                SQLiteDatabase db = mDBHelper.getWritableDatabase();
                int count = db.update(SkittleDatabaseContract.TABLE_MOVIES, values, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                return count;
            } catch (Exception e){
                return 0;
            } finally {
                mLock.unlock();
            }
        }
        if ((mUriMatcher.match(uri) == SERIE_CORRECT_URI_CODE) && (null != values)){
            mLock.lock();
            try {
                SQLiteDatabase db = mDBHelper.getWritableDatabase();
                int count = db.update(SkittleDatabaseContract.TABLE_SERIES, values, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                return count;
            } catch (Exception e){
                return 0;
            } finally {
                mLock.unlock();
            }
        }
        return 0;
    }
}
