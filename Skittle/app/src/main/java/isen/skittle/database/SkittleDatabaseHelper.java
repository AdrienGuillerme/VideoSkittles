package isen.skittle.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by adrie on 05/03/2017.
 */

public class SkittleDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "skittle.db";
    private static final int DATABASE_VERSION = 1;

    public SkittleDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SkittleDatabaseContract.TABLE_MOVIES_CREATE_SCRIPT);
        db.execSQL(SkittleDatabaseContract.TABLE_SERIES_CREATE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + SkittleDatabaseContract.TABLE_MOVIES);
        db.execSQL("DROP TABLE IF EXISTS" + SkittleDatabaseContract.TABLE_SERIES);
        onCreate(db);
    }
}
