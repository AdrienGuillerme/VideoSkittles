package isen.skittle.database;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by adrie on 05/03/2017.
 */

public class SkittleDatabaseContract implements BaseColumns {

    // Field names
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String NAME = "name";
    public static final String POSTER = "posterPath";
    public static final String BACKDROP = "backdropPath";
    public static final String AIRDATE = "firstAirDate";
    public static final String OVERVIEW = "overview";
    public static final String RATING = "rating";
    public static final String COUNTRIES = "countries";
    public static final String GENRES = "genres";
    public static final String RUNTIME = "runtime";
    public static final String EPISODERUNTIME = "episodeRuntime";
    public static final String ACTORS = "actors";
    public static final String WRITER = "writer";
    public static final String DIRECTOR = "director";
    public static final String NBSEASONS = "nbSeasons";
    public static final String NBEPISODES = "nbEpisodes";
    public static final String VIEWPOINT = "lastEpisodeSeen";



    // Table name
    public static final String TABLE_MOVIES = "movies";
    public static final String TABLE_SERIES = "series";

    // Table scripts creation
    private static final String TABLE_GENERIC_CREATE_SCRIPT_PREFIX = "CREATE TABLE IF NOT EXISTS ";
    private static final String TABLE_MOVIES_CREATE_SCRIPT_SUFFIX = "(" + _ID + " INTEGER PRIMARY KEY, " +
            ID + " INTEGER, " +
            TITLE + " INTEGER, " +
            POSTER + " TEXT NOT NULL, "+
            BACKDROP + " TEXT NOT NULL, "+
            OVERVIEW + " TEXT NOT NULL, "+
            RATING + " TEXT NOT NULL, "+
            AIRDATE + " TEXT NOT NULL, "+
            COUNTRIES + " TEXT NOT NULL, "+
            RUNTIME + " INTEGER, "+
            GENRES + " TEXT NOT NULL, "+
            ACTORS + " TEXT NOT NULL, "+
            DIRECTOR + " TEXT NOT NULL)" ;

    private static final String TABLE_SERIES_CREATE_SCRIPT_SUFFIX = "(" + _ID + " INTEGER PRIMARY KEY, " +
            ID + " INTEGER, " +
            NAME + " TEXT NOT NULL, " +
            POSTER + " TEXT NOT NULL, "+
            BACKDROP + " TEXT NOT NULL, "+
            OVERVIEW + " TEXT NOT NULL, "+
            RATING + " TEXT NOT NULL, "+
            AIRDATE + " TEXT NOT NULL, "+
            EPISODERUNTIME + " INTEGER, "+
            NBSEASONS + " INTEGER, "+
            NBEPISODES + " INTEGER, "+
            VIEWPOINT + " INTEGER, "+
            COUNTRIES + " INTEGER, "+
            GENRES + " TEXT NOT NULL, "+
            ACTORS + " TEXT NOT NULL, " +
            WRITER + " TEXT NOT NULL)";


    public static final String TABLE_MOVIES_CREATE_SCRIPT = TABLE_GENERIC_CREATE_SCRIPT_PREFIX +
            TABLE_MOVIES + TABLE_MOVIES_CREATE_SCRIPT_SUFFIX;

    public static final String TABLE_SERIES_CREATE_SCRIPT = TABLE_GENERIC_CREATE_SCRIPT_PREFIX +
            TABLE_SERIES + TABLE_SERIES_CREATE_SCRIPT_SUFFIX;

    // The projections
    public static final String[] PROJECTION_FULL_MOVIES = new String[]{
            _ID,
            ID,
            TITLE,
            POSTER,
            BACKDROP,
            OVERVIEW,
            RATING,
            AIRDATE,
            COUNTRIES,
            RUNTIME,
            GENRES,
            ACTORS,
            DIRECTOR
    };

    public static final String[] PROJECTION_FULL_SERIES = new String[]{
            _ID,
            ID,
            NAME,
            POSTER,
            BACKDROP,
            OVERVIEW,
            RATING,
            AIRDATE,
            EPISODERUNTIME,
            COUNTRIES,
            NBSEASONS,
            NBEPISODES,
            VIEWPOINT,
            COUNTRIES,
            GENRES,
            ACTORS,
            WRITER
    };


    // Selections
    public static final String SELECTION_BY_ID = _ID + "=?";
    public static final String SELECTION_BY_TMDBID = ID + "=?";
    public static final String SELECTION_BY_TITLE = TITLE + "=?";
    public static final String SELECTION_BY_NAME = NAME + "=?";


    // Content Provider stuff
    public static final String CONTENT_PROVIDER_MOVIES_AUTHORITY = "isen.MoviesAuthority";
    public static final Uri MOVIES_URI = Uri.parse("content://" + CONTENT_PROVIDER_MOVIES_AUTHORITY + "/" + TABLE_MOVIES);
    public static final String MOVIES_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.skittle.movies";

    public static final Uri SERIES_URI = Uri.parse("content://" + CONTENT_PROVIDER_MOVIES_AUTHORITY + "/" + TABLE_SERIES);
    public static final String SERIES_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.skittle.series";

}
