package isen.skittle;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import isen.skittle.holder.ShortViewHolder;
import isen.skittle.ui.fragments.ShortFragment;
import isen.skittle.utils.Constants;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        String query = "";
        // Retrieve the login passed as parameter
        final Intent intent = getIntent();
        if (null != intent){
            final Bundle extras = intent.getExtras();
            if ((null != extras) && (extras.containsKey(Constants.General.SEARCH))){
                // Retrieve the query
                query = extras.getString(Constants.General.SEARCH);
                // Set as ActionBar subtitle
                getSupportActionBar().setSubtitle(query);
            }
        }

        if (savedInstanceState == null) {
            final FragmentTransaction transaction = getFragmentManager().beginTransaction();
            final ShortFragment fragment = new ShortFragment();
            final Bundle bundle = new Bundle();
            bundle.putString(Constants.General.SEARCH, query);
            fragment.setArguments(bundle);
            transaction.add(R.id.activity_search, fragment).commit();
        }
    }
}